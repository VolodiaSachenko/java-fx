package ua.ithillel.javafx.homework13.userdata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class UserDatabaseParser implements UserChecker, Parser {

    private static final String FILE_PATH = "src/main/resources/ua/ithillel/javafx/login.txt";

    @Override
    public boolean checkUser(String checkUsername, String checkPassword) {
        List<String> text = parseTxt();
        for (String line : text) {
            String[] splitColumns = line.split("\\|");
            String username = splitColumns[0].replaceAll("username=", "");
            String password = splitColumns[1].replaceAll("password=", "");
            if (username.equals(checkUsername) && password.equals(checkPassword)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<String> parseTxt() {
        List<String> text = new ArrayList<>();
        try {
            text = Files.readAllLines(Path.of(FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        return text;
    }
}
