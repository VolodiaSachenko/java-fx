package ua.ithillel.javafx.homework13.userdata;

import java.util.List;

public interface Parser {

    List<String> parseTxt();

}
