package ua.ithillel.javafx.homework13.userdata;

public interface UserChecker {

    boolean checkUser(String checkUsername, String checkPassword);

}
