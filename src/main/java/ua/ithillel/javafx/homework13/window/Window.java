package ua.ithillel.javafx.homework13.window;

import javafx.scene.layout.GridPane;

public interface Window {

    GridPane createGridPane();

}
