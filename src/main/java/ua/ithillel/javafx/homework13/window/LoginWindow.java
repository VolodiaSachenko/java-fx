package ua.ithillel.javafx.homework13.window;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import ua.ithillel.javafx.homework13.userdata.UserDatabaseParser;

public class LoginWindow implements Window {

    @Override
    public GridPane createGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(10));
        createLoginForm(gridPane);
        return gridPane;
    }

    private void createLoginForm(GridPane gridPane) {
        gridPane.add(createHeaderText(), 0, 0);

        gridPane.add(createContentText("User Name:"), 0, 1);
        TextField txtUser = createTextField();
        gridPane.add(txtUser, 1, 1);

        gridPane.add(createContentText("Password:"), 0, 2);
        PasswordField passwordField = createPasswordField();
        gridPane.add(passwordField, 1, 2);

        Button loginButton = createLoginButton();
        loginButtonAction(loginButton, txtUser, passwordField, gridPane);
        gridPane.add(loginButton, 1, 3);
        GridPane.setHalignment(loginButton, HPos.RIGHT);
    }

    private PasswordField createPasswordField() {
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("password");
        return passwordField;
    }

    private TextField createTextField() {
        TextField txtUser = new TextField();
        txtUser.setPromptText("username");
        return txtUser;
    }

    private Text createHeaderText() {
        Text login = new Text("Login");
        login.setFont(Font.font("Helvetica", FontWeight.BOLD, 24));
        return login;
    }

    private Label createContentText(String value) {
        Label text = new Label(value);
        text.setFont(Font.font("Helvetica", 15));
        return text;
    }

    private Button createLoginButton() {
        Button button = new Button("Login");
        button.getStyleClass().setAll("btn", "btn-success");
        return button;
    }

    private Node getNodeFromGridPane(GridPane gridPane) {
        Node gridNode = null;
        ObservableList<Node> children = gridPane.getChildren();
        for (Node node : children) {
            if (GridPane.getRowIndex(node) == 3 && GridPane.getColumnIndex(node) == 1) {
                gridNode = node;
                break;
            }
        }
        return gridNode;
    }

    private void openAccess(GridPane gridPane) {
        Text welcome = new Text("Welcome to app");
        welcome.setFont(Font.font("Helvetica", FontWeight.BOLD, 24));
        gridPane.getChildren().clear();
        gridPane.add(welcome, 0, 0);
        gridPane.setAlignment(Pos.CENTER);
        System.out.println("Login is correct");
    }

    private void closeAccess(GridPane gridPane, Button button) {
        Text text = new Text("Incorrect login");
        text.setFont(Font.font("Helvetica", FontWeight.BOLD, 15));
        gridPane.getChildren().remove(button);
        Node getIncorrectText = getNodeFromGridPane(gridPane);
        if (getIncorrectText == null) {
            gridPane.add(text, 1, 3);
            System.out.println("Incorrect login or password");
        }
        gridPane.add(button, 1, 4);
    }

    private void loginButtonAction(Button loginButton, TextField username, PasswordField password, GridPane pane) {
        loginButton.setOnAction(actionEvent -> {
            String getUsername = username.getText();
            String getPassword = password.getText();
            UserDatabaseParser databaseParser = new UserDatabaseParser();
            if (databaseParser.checkUser(getUsername, getPassword)) {
                openAccess(pane);
            } else {
                closeAccess(pane, loginButton);
            }
        });
    }
}
