package ua.ithillel.javafx.homework13;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;
import ua.ithillel.javafx.homework13.window.LoginWindow;

/**
 * @author Volodia Sachenko
 */

public class Homework13 extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Login form");
        LoginWindow loginWindow = new LoginWindow();
        Scene scene = new Scene(loginWindow.createGridPane(), 300, 320);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        stage.setScene(scene);
        stage.show();
    }
}
